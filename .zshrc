# Path to your oh-my-zsh installation.
  export ZSH=~/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME=""

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
#plugins=(git)

## own var
# path relative to user home
INBOX_PATH="file/@inbox/"

source $ZSH/oh-my-zsh.sh
# User configuration

export PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:~/file/bin"
export EDITOR="nvim"
export PAGER="vimpager"
export GIT_EDITOR="nvim"
# export MANPATH="/usr/local/man:$MANPATH"


# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

####### MY ALIAS ########

alias zshconfig="nvim ~/.zshrc"
alias vimconfig="nvim ~/.vimrc"
alias tmuxconfig="nvim ~/.tmux.conf"
alias ohmyzsh="nvim ~/.oh-my-zsh"
alias android-studio="sh ~/software/dev/ide/android-studio/bin/studio.sh"
alias z_readme="nvim ~/file/script/z-master/README"
alias mountvps="mkdir ~/vps; sshfs yatera@vps261690.ovh.net: ~/vps"
alias tmux_androidstudio="tmux new-session -s androidstudio -d && tmux send-keys -t androidstudio 'android-studio' ENTER"
alias tmux_clipit="tmux new-session -s clipit -d clipit"
alias tmux_ks="tmux kill-session -t"
alias tmux_as="tmux attach-session -t"
alias tmux_ns="tmux new-session -t"
#pacman
alias pacman="sudo pacman"
alias update_arch="sudo pacman -Syu; yaourt -Syu --aur; sudo pacman -Qdtq; yaourt -Qdtq --aur"
alias update_debian="sudo apt-get update; sudo apt-get upgrade; sudo apt-get autoremove"
alias install_vim_plug="curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
alias install_vimpager_arch="git clone git://github.com/rkitover/vimpager;
cd vimpager;
sudo make install;
cd ..; 
rm -r vimpager"
alias install_vimpager_debian="git clone git://github.com/rkitover/vimpager;
cd vimpager;
sudo make install-deb;
cd ..;
rm -r vimpager"
#ranger
alias ranger='ranger --choosedir=$HOME/rangerdir; LASTDIR=`cat $HOME/rangerdir`; cd "$LASTDIR"; rm "$HOME/rangerdir"'
#neovim 
alias vim="nvim"
alias decryptDropbox="encfs ~/file/Dropbox/encrypted ~/dropboxEncrypted"

alias cl="clear"
alias ..="cd .."
alias ...="cd ../.."

if [[ "$OSTYPE" == "darwin"* ]]; then
	#mac
	alias rm="trash"
else
        # linux
	alias rm="trash-put"
fi
alias cl="clear"
alias ..="cd .."
alias ...="cd ../.."

alias adbd="adb devices"
alias adbph="adb push"
alias adbpl="adb pull"
alias adbks="adb kill-server"
alias adbss="adb start-server"
alias adbs="adb shell"
alias adblc="adb logcat"

alias agu="sudo apt-get update"
alias agi="sudo apt-get install"
alias agr="sudo apt-get remove"
alias agp="sudo apt-get purge"
alias agar="sudo apt-get autoremove"
alias agug="sudo apt-get upgrade"

alias ll="ls -lh"
alias la="ls -a"
alias lla="la -lha"
#copy to clipboard
alias cc="xclip -selection clipboard"
#paste from clipboard
alias pc="xclip -o -selection clipboard"

alias rscp="rsync -ahP"

alias top="htop"

alias gcac="git commit --amend -c HEAD"
alias gca="git commit --amend -C HEAD"
alias gph="git push"
alias gphf="git push -f"
alias gpl="git pull"
alias gplf="git pull -f"
alias gplnoff="git pull --no-ff"
alias gm="git merge --no-ff"
alias gmnff="git merge --no-ff"
alias ga="git add"
alias gaa="git add --all"
alias gc="git commit"
alias gcm="git commit -m"
alias grb="git rebase"
alias grba="git rebase --abord"
alias grbc="git rebase --continue"
alias gs="git status"
alias gst="git stash"
alias gsta="git stash apply"
alias gstl="git stash list"
alias gl="git log --decorate --oneline --graph"
alias gf="git fetch"
#liquid prompt
alias liquidprompt="source ~/file/script/liquidprompt/liquidprompt"
#android studio
alias android-studio="~/software/dev/ide/android-studio/bin/studio.sh"
#ntfs
alias mount-ntfs="sudo /usr/local/bin/ntfs-3g /dev/disk2s1 /Volumes/NTFS -olocal -oallow_other"
#checkstyle
alias checkstyle="java -jar ~/software/dev/lint/checkstyle.jar -c /google_checks.xml"
#google java format
alias googleJavaFormat="java -jar ~/software/dev/lint/google-java-format.jar"
#pmd
alias pmd="sh ~/software/dev/lint/pmd/bin/run.sh pmd"

##  FUNCTIONS ##

#tmux prompt a dialog,
#the user can attach the terminal to default tmux session
#the default session name is the user name
tmuxDefaultSessionAttachPromptFunc() {
	if [[ "$TERM" != "screen-256color" ]]; then
		echo "Attach to default tmux session?(Y/n)"
		read -t 3 tmux_launch
		#if timeout, no attach to default tmux session
		if [ $? != 0 ]; then
			return
		fi
		#if empty or 'y' attch to default tmux session
		if [ -z "$tmux_launch" ] || [ "$tmux_launch" = "y" ]; then
			tmux attach -t "$USER" || tmux new-session -s "$USER"
		fi
		#otherwise do nothing, no attach to default tmux session
	fi
}

mkcd() {
	mkdir $1 && cd $1
}

mv2inbox() {
	mv $@ "$HOME/$INBOX_PATH"
}

cp2inbox() {
	cp $@ "$HOME/$INBOX_PATH"
}

cpr2inbox() {
	cp -r $@ "$HOME/$INBOX_PATH"
}

###### EXECUTE ON START ########

#liquid prompt
liquidprompt
# z
source ~/file/script/z-master/z.sh
tmuxDefaultSessionAttachPromptFunc
