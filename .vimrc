call plug#begin('~/.vim/plugged')
Plug 'https://github.com/rking/ag.vim'
Plug 'https://github.com/scrooloose/nerdtree'
Plug 'https://github.com/kien/ctrlp.vim'
Plug 'https://github.com/tpope/tpope-vim-abolish'
Plug 'https://github.com/Xuyuanp/nerdtree-git-plugin'
Plug 'https://github.com/rkitover/vimpager'
Plug 'https://github.com/tpope/vim-surround'
Plug 'https://github.com/tpope/vim-repeat'
Plug 'https://github.com/kana/vim-textobj-user'
Plug 'https://github.com/kana/vim-textobj-entire'
Plug 'https://github.com/bronson/vim-visual-star-search'
Plug 'https://github.com/wincent/command-t'
Plug 'https://github.com/JamshedVesuna/vim-markdown-preview'
Plug 'https://github.com/vim-syntastic/syntastic'
Plug 'https://github.com/keith/swift.vim'
call plug#end()

filetype plugin indent on
set nocompatible
set history=10000
set ignorecase
set smartcase
set incsearch
set number relativenumber
syntax on
" Search down into subfolders
" provieds tag-completion for all file-related tasks
set path+=**
" Display all matching files when we tab complete
set wildmenu
set showcmd
" map leader
let mapleader = "\<Space>"
"copy to system clipboard
vnoremap <Leader>yc "+y
nnoremap <Leader>yc "+y
nnoremap <Leader>pc "+p
nnoremap <Leader>Pc "+P

nnoremap h <NOP>
nnoremap l <NOP>
nnoremap <esc> <NOP>
inoremap <esc> <NOP>
vnoremap <esc> <NOP>
inoremap jk <esc>
inoremap kj <esc>
nnoremap <M-j> ]mzz
nnoremap <M-k> [mzz
vnoremap <M-j> ]mzz
vnoremap <M-k> [mzz
"for ideamvim Alt is not M but A
nnoremap <A-j> ]mzz
nnoremap <A-k> [mzz
vnoremap <A-j> ]mzz
vnoremap <A-k> [mzz
"method jump mac (fn alt not work on mac)
nnoremap <Leader>j ]mzz
nnoremap <Leader>k [mzz
vnoremap <Leader>j ]mzz
vnoremap <Leader>k [mzz

nnoremap <C-J> }zz
nnoremap <C-K> {zz
vnoremap <C-J> }zz
vnoremap <C-K> {zz
nnoremap <Leader>ch :tjump *<C-r><C-w>*
nnoremap <C-H> :bp<CR>
nnoremap <C-L> :bn<CR>
"empty line
nnoremap <Enter>  o<ESC>
nnoremap <S-Enter> O<ESC>

if has("clipboard")
	set clipboard=unnamed
	set clipboard=unnamedplus
endif

if has('nvim')
        tnoremap <Esc> <NOP>
        tnoremap jk <C-\><C-n>
        tnoremap kj <C-\><C-n>
endif

" PLUGINS SETTINGS "

" overcome limit imposed by max height
let g:ctrlp_match_window = 'results:100' 

" syntastic settings
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_mode_map = { 'mode': 'passive'}
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0
let g:syntastic_swift_checkers = ['swiftpm', 'swiftlint']
"markdow preview plugin options
let vim_markdown_preview_hotkey='<Leader>md'
let vim_markdown_preview_github=1
"ctrlp plugin options
let g:ctrl_map =  '<Leader>pp'
let g:ctrlp_cmd = 'CtrlP'

" THEME 

"cursor
let &t_SI .= "\e[5 q"
let &t_SR .= "\e[3 q"
let &t_EI .= "\e[1 q"

" theme and colors
colorscheme desert
set guifont=Monospace\ 14

" FUNCTIONS "

function! GotoJump()
  jumps
    let j = input("Please select your jump: ")
      if j != ''
          let pattern = '\v\c^\+'
	  if j =~ pattern
		  let j = substitute(j, pattern, '', 'g')
		  execute "normal " . j . "\<c-i>"
         else
                   execute "normal " . j . "\<c-o>"
         endif
       endif
endfunction
